#!/usr/bin/env bash

set +xe

CI_REGISTRY=registry.gitlab.com

#docker logout
#
#docker login ${CI_REGISTRY}

docker build -t "${CI_REGISTRY}/sisicmbio/pgbadger/app:develop" .

docker push  "${CI_REGISTRY}/sisicmbio/pgbadger/app:develop"

