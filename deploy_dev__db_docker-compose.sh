#!/usr/bin/env bash

usage()
{
    echo "========================================================================================================"
    echo "Selecione em qual ambiente ira instalar:                           "
    echo ""
    echo "      -dev   | dev                 Ambiente DEV - Ambiente local do Desenvolvedor           "
    echo "      -test  | test                Ambiente TEST - Ambiente para execução dos testes automatizados."
    echo "========================================================================================================"
}

case $1 in
    -dev| dev )
        export PDCI_FILE_ENV_DOCKER=./.env.docker.dev
        export PDCI_PROJECT_NAME=dev

    ;;
    -test | test )
        export PDCI_FILE_ENV_DOCKER=./.env.docker.test
        export PDCI_PROJECT_NAME=dev_test
    ;;
    -h | --help )
        usage
        exit
    ;;
    * )
        usage
        exit 1
esac

echo
echo
echo
echo "Informe seu login e senha para o registry.gitlab.com:"
echo
read -p 'Username: ' PDCI_CI_REGISTRY_USER
read -sp 'Password: ' PDCI_CI_REGISTRY_PASSWORD
echo

if [[ ! -z ${PDCI_CI_REGISTRY_PASSWORD} && ! -z ${PDCI_CI_REGISTRY_USER} ]]; then

echo "Realizando login com  ${PDCI_CI_REGISTRY_USER} em registry.gitlab.com"
docker logout

echo "${PDCI_CI_REGISTRY_PASSWORD}" | docker   login --username ${PDCI_CI_REGISTRY_USER} --password-stdin  registry.gitlab.com

else

echo "Utilizndo cache de login"
fi

echo ""
echo "**************************************************************************************"
echo "           Deploy do banco de dados para o ambiente DEV                               "
echo "**************************************************************************************"
echo ""
export _porta_random=$(_random=$((20034 % 1000)) &&  printf "%03d" ${_random})

echo "PDCI_PROJECT_NAME => ${PDCI_PROJECT_NAME:-dev} "

docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__db_docker-compose.yml config
docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__db_docker-compose.yml pull
docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__db_docker-compose.yml rm -f -s -v

docker  volume rm  ${PDCI_PROJECT_NAME:-dev}_data_pgbadger_db -f || true
docker  volume rm  ${PDCI_PROJECT_NAME:-dev}_data_pgbadger_tbs_geo_db -f  || true
docker  volume rm  ${PDCI_PROJECT_NAME:-dev}_data_pgbadger_index_ssd_db -f  || true

docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__db_docker-compose.yml up -d
#docker-compose -p ${PDCI_PROJECT_NAME:-dev} -f dev__db_docker-compose.yml logs -f pgbadger_ifd
