DO
$
BEGIN
 IF NOT EXISTS (
     SELECT                       -- SELECT list can stay empty for this
     FROM   pg_catalog.pg_roles
     WHERE  rolname = 'usr_pgbadger') THEN

   create user usr_pgbadger with password 'usr_pgbadger';
 END IF;

--  IF NOT  EXISTS (
--      SELECT -- SELECT list can stay empty for this
--      FROM information_schema.tables  WHERE table_schema='pgbadger'
--  )
--  THEN
--   CREATE SCHEMA IF NOT EXISTS pgbadger;
--
--  END IF;
 CREATE SCHEMA IF NOT EXISTS pgbadger;

 grant all on all tables in schema pgbadger to usr_pgbadger;
 grant usage on all sequences in schema pgbadger to usr_pgbadger;
 grant execute on all functions in schema pgbadger to usr_pgbadger;

 grant all on all tables in schema pgbadger to usr_jenkins;
 grant usage on all sequences in schema pgbadger to usr_jenkins;
 grant execute on all functions in schema pgbadger to usr_jenkins;

END;
$;