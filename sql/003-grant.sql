GRANT ALL ON SCHEMA pgbadger TO usr_pgbadger;
--GRANT ALL ON SCHEMA pgbadger TO PUBLIC;
--alter role usr_pgbadger set search_path = pgbadger, public, postgis ;

GRANT ALL ON all tables in SCHEMA pgbadger TO usr_pgbadger ;
GRANT ALL ON all functions in SCHEMA pgbadger TO usr_pgbadger;
GRANT ALL ON all sequences in SCHEMA pgbadger TO usr_pgbadger;
--
-- PostgreSQL database dump complete
--

